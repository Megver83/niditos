import openpyxl
from openpyxl.styles import Alignment, Font
from .database import Usuario, Registro
from datetime import datetime, timedelta
import calendar


def generar_reporte_horas(ruta_del_archivo, rol, mes=datetime.now().strftime("%Y-%m")):
    # Crear un nuevo libro de Excel
    libro_excel = openpyxl.Workbook()

    # Configurar encabezados de la hoja "General"
    hoja_general = libro_excel.active
    hoja_general.title = 'General'
    hoja_general['A1'].value = 'Nombre'
    hoja_general['B1'].value = 'Horas'

    # Configurar estilo para encabezados
    for cell in hoja_general['1:1']:
        cell.font = Font(bold=True)
        cell.alignment = Alignment(horizontal='center')

    # Configurar ancho de columnas
    hoja_general.column_dimensions['A'].width = 32

    # Obtener la lista de usuarios
    usuarios = Usuario.select().where(Usuario.rol == rol)

    # Iterar sobre los usuarios para calcular las horas y agregar a la hoja "General"
    for usuario in usuarios:
        # Obtener los registros del usuario para el mes especificado
        registros_usuario = registros_usuario_por_mes(usuario, mes)

        horas_totales = calcular_horas_usuario(usuario, registros_usuario)
        hoja_general.append([f"{usuario.nombres} {usuario.apellidos}", round(horas_totales, 2)])

        # Crear una hoja para cada usuario
        hoja_usuario = libro_excel.create_sheet(title=usuario.rut)

        # Configurar encabezados de la hoja del usuario
        hoja_usuario['A1'].value = 'Fecha'
        hoja_usuario['B1'].value = 'Movimiento'

        # Configurar estilo para encabezados de la hoja del usuario
        for cell in hoja_usuario['1:1']:
            cell.font = Font(bold=True)
            cell.alignment = Alignment(horizontal='center')

        # Configurar ancho de columnas para la hoja del usuario
        hoja_usuario.column_dimensions['A'].width = 24
        hoja_usuario.column_dimensions['B'].width = 16

        # Agregar los registros a la hoja del usuario
        for registro in registros_usuario:
            hoja_usuario.append([registro.fecha, registro.movimiento])

    # Guardar el libro de Excel
    libro_excel.save(ruta_del_archivo)


def calcular_horas_usuario(usuario, registros):
    ultimo_movimiento = None
    total = timedelta()
    for registro in registros:
        if registro.movimiento == "Ingreso":
            ultimo_ingreso = registro.fecha
        elif ultimo_movimiento == "Ingreso" and registro.movimiento == "Salida":
            ultima_salida = registro.fecha
            total += ultima_salida - ultimo_ingreso
        ultimo_movimiento = registro.movimiento

    # Devolver el número de horas
    return total.total_seconds() / 3600.0


def registros_usuario_por_mes(usuario, mes):
    # Calcular las fechas de inicio y fin del mes
    year, month = map(int, mes.split('-'))
    _, last_day = calendar.monthrange(year, month)

    fecha_inicio_mes = f"{mes}-01 00:00:00"
    fecha_fin_mes = f"{mes}-{last_day} 23:59:59"

    return Registro.select().join(Usuario).where(
        (Registro.rut_usuario == usuario) &
        (Registro.fecha >= fecha_inicio_mes) &
        (Registro.fecha <= fecha_fin_mes)
    ).order_by(Registro.fecha)
