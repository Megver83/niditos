from .utils import MOVEMENTS, ROLES, db_path
import peewee
from datetime import datetime

# Conectamos a la base de datos SQLite
db_file_path = db_path()
db = peewee.SqliteDatabase(db_file_path)


# Definimos el modelo de Usuario
class Usuario(peewee.Model):
    nombres = peewee.TextField(null=False)
    apellidos = peewee.TextField(null=False)
    rut = peewee.TextField(null=False, unique=True)
    rol = peewee.TextField(choices=ROLES, null=False)
    photo = peewee.BlobField(null=False)

    class Meta:
        database = db


# Definimos el modelo de Registro
class Registro(peewee.Model):
    fecha = peewee.DateTimeField(default=datetime.now, null=False)
    rut_usuario = peewee.ForeignKeyField(Usuario, field='rut', backref='registros', null=False)
    movimiento = peewee.TextField(choices=MOVEMENTS, null=False)

    class Meta:
        database = db


def initialize_database():
    # Creamos las tablas en la base de datos
    with db:
        db.create_tables([Usuario, Registro])


def execute_sql(query: str):
    with db:
        db.execute_sql(query)
