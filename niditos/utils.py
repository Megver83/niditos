from platformdirs import user_config_dir
from io import BytesIO
from datetime import datetime, timedelta
import os

# Nombre de la aplicación
APPNAME = "Niditos"

# Configuramos la ruta de los archivos de datos en el directorio de configuración del usuario
CONFIG_DIR = user_config_dir(APPNAME, roaming=True)

# Base de datos
DB_NAME = "data.db"
DB_PATH = os.path.join(CONFIG_DIR, DB_NAME)
ROLES = ["Docente", "Alumno"]
MOVEMENTS = ["Ingreso", "Salida"]


def db_path():
    os.makedirs(CONFIG_DIR, exist_ok=True)
    return DB_PATH


def filename2blob(filename: str):
    with open(filename, 'rb') as file:
        blob = file.read()
    return blob


def file2bytesio(file_object):
    return BytesIO(file_object)


def preprocesar_campos(nombres: str, apellidos: str, rut: str):
    if not f"{nombres.replace(' ', '')}{apellidos.replace(' ', '')}".isalpha():
        raise ValueError("Los nombres y apellidos deben contener solo caracteres alfabéticos")

    nombres_cap = nombres.title()
    apellidos_cap = apellidos.title()
    rut_up = rut.upper()

    return nombres_cap, apellidos_cap, rut_up


def seleccionar_registros_por_dia(fecha=datetime.now(), desc=False):
    from .database import Registro

    # Calcular las fechas de inicio y fin del día
    fecha_inicio_dia = fecha.replace(hour=0, minute=0, second=0, microsecond=0)
    fecha_fin_dia = fecha_inicio_dia + timedelta(days=1)

    # Seleccionar y ordenar los registros por fecha
    registros_del_dia = (
        Registro.select()
        .where((Registro.fecha >= fecha_inicio_dia) & (Registro.fecha < fecha_fin_dia))
        .order_by(Registro.fecha.desc() if desc else Registro.fecha)
    )

    return registros_del_dia


def backup_initialfile(timestamp=datetime.now().strftime("%Y-%m-%d %H:%M:%S")):
    return f"{APPNAME}_backup_{timestamp}.db"
