import tkinter as tk
from tkinter import ttk, messagebox, filedialog
from PIL import Image, ImageTk
import threading
import webbrowser
import os
import shutil
import importlib_resources
from .database import (
    Usuario,
    Registro,
    initialize_database,
    execute_sql
)
from .utils import (
    APPNAME,
    MOVEMENTS,
    ROLES,
    DB_PATH,
    filename2blob,
    file2bytesio,
    preprocesar_campos,
    seleccionar_registros_por_dia,
    backup_initialfile
)
from .facerec import recognize_from_webcam
from .report import generar_reporte_horas
from . import __version__


class InterfazNiditos:
    def __init__(self, master):
        self.master = master
        master.title(APPNAME)

        # Construir la ruta relativa al icono
        icon_filename = "icon.ico"
        icon_path = importlib_resources.files(__name__) / icon_filename

        # Verificar si el archivo de icono existe
        self.icon = None
        if os.path.exists(icon_path):
            # Cargar el icono
            self.icon = ImageTk.PhotoImage(Image.open(icon_path))

            # Configurar el icono en la ventana principal
            master.iconphoto(True, self.icon)
        else:
            # Mostrar un mensaje de advertencia si el archivo de icono no se encuentra
            print(f"Advertencia: No se encontró el archivo de icono en la ruta '{icon_path}'.")

        self.reconocimiento_facial_thread = threading.Thread()
        self.radios_movimiento = []

        # Inicializar la base de datos
        initialize_database()

        # Crear pestañas
        self.tabs = ttk.Notebook(master)
        self.tab_registrar = ttk.Frame(self.tabs)
        self.tab_reporte = ttk.Frame(self.tabs)
        self.tab_usuarios = ttk.Frame(self.tabs)
        self.tab_almacenamiento = ttk.Frame(self.tabs)
        self.tab_acerca_de = ttk.Frame(self.tabs)
        self.tabs.add(self.tab_registrar, text="Registro")
        self.tabs.add(self.tab_reporte, text="Reporte")
        self.tabs.add(self.tab_usuarios, text="Usuarios")
        self.tabs.add(self.tab_almacenamiento, text="Almacenamiento")
        self.tabs.add(self.tab_acerca_de, text="Acerca de")
        self.tabs.pack(expand=1, fill="both")

        # Pestaña "Registrar"
        self.cargar_pestana_registro()

        # Pestaña "Reporte"
        self.cargar_pestana_reporte()

        # Pestaña "Usuarios"
        self.cargar_pestana_usuarios()

        # Pestaña "Almacenamiento"
        self.cargar_pestana_almacenamiento()

        # Cargar contenido de la pestaña "Acerca de"
        self.cargar_pestana_acerca_de()

    def cargar_pestana_almacenamiento(self):
        # Crear botones e indicadores para la pestaña "Almacenamiento"
        lbl_backup = tk.Label(self.tab_almacenamiento, text="Copia de seguridad")
        btn_exportar_db = tk.Button(self.tab_almacenamiento, text="Exportar", command=self.exportar_base_datos)
        btn_importar_db = tk.Button(self.tab_almacenamiento, text="Importar", command=self.importar_base_datos)
        self.lbl_tamano_db = tk.Label(self.tab_almacenamiento, text="Tamaño de la Base de Datos: ")
        btn_limpiar_registros = tk.Button(self.tab_almacenamiento, text="Limpiar Registros", command=self.confirmar_limpiar_registros)

        lbl_backup.pack()
        btn_exportar_db.pack(pady=10)
        btn_importar_db.pack(pady=10)
        self.lbl_tamano_db.pack()
        btn_limpiar_registros.pack(pady=10)

        # Actualizar el indicador del tamaño de la base de datos
        self.actualizar_tamano_db()

    def exportar_base_datos(self):
        # Obtener la ruta de destino del archivo de base de datos
        file_path = filedialog.asksaveasfilename(
            initialfile=backup_initialfile(),
            title="Exportar Base de Datos",
            defaultextension=".db",
            filetypes=[("Archivos de Base de Datos SQLite", "*.db")],
        )

        # Verificar si el usuario canceló el diálogo
        if not file_path:
            return

        try:
            # Copiar el archivo de base de datos a la nueva ubicación
            shutil.copy2(DB_PATH, file_path)

            messagebox.showinfo("Exportado", "La base de datos se ha exportado exitosamente.")
        except Exception as e:
            messagebox.showerror("Error", f"No se pudo exportar la base de datos: {str(e)}")

    def importar_base_datos(self):
        # Obtener la ruta del archivo de base de datos a importar
        file_path = filedialog.askopenfilename(
            title="Importar Base de Datos",
            filetypes=[("Archivos de Base de Datos SQLite", "*.db")],
        )

        # Verificar si el usuario canceló el diálogo
        if not file_path:
            return

        try:
            # Copiar el archivo de base de datos importado a la ubicación original
            shutil.copy2(file_path, DB_PATH)

            # Reiniciar la aplicación
            self.master.destroy()
            main()
        except Exception as e:
            messagebox.showerror("Error", f"No se pudo importar la base de datos: {str(e)}")

    def confirmar_limpiar_registros(self):
        # Mostrar un cuadro de diálogo de confirmación antes de limpiar los registros
        respuesta = messagebox.askyesno("Confirmar Limpiar Registros", "¿Está seguro de que desea limpiar todos los registros? Esta acción no se puede deshacer.")

        if respuesta:
            # Limpiar los registros de la base de datos
            execute_sql("DELETE FROM registro")

            # Actualizar la vista de registros
            self.cargar_registros()
            self.actualizar_meses_con_registros()

            # Mostrar un mensaje de éxito
            messagebox.showinfo("Registros Limpiados", "Se han limpiado todos los registros correctamente.")

            # Actualizar el indicador del tamaño de la base de datos
            self.actualizar_tamano_db()

    def actualizar_tamano_db(self):
        # Obtener el tamaño del archivo de la base de datos
        tamano_bytes = os.path.getsize(DB_PATH)
        # Convertir bytes a megabytes
        tamano_mb = tamano_bytes / (1024 * 1024)
        # Actualizar el texto del indicador
        self.lbl_tamano_db.config(text=f"Tamaño de la Base de Datos: {tamano_mb:.2f} MB")

    def cargar_pestana_reporte(self):
        # Crear Label para mostrar el texto
        lbl_reporte_texto = tk.Label(self.tab_reporte, text="Reporte completo del mes")
        lbl_reporte_texto.pack(pady=10)

        # Crear Combobox para seleccionar el mes
        self.combobox_meses = ttk.Combobox(self.tab_reporte, state="readonly", width=10)
        self.actualizar_meses_con_registros()

        self.combobox_meses.pack(pady=10)

        # Crear radio buttons para seleccionar el rol
        self.rol_seleccionado = tk.StringVar(self.tab_reporte, value=ROLES[0])  # Establecer el primer rol como predeterminado
        for rol in ROLES:
            rb_rol = tk.Radiobutton(self.tab_reporte, text=rol, variable=self.rol_seleccionado, value=rol)
            rb_rol.pack()

        # Botón "Exportar"
        btn_exportar = tk.Button(self.tab_reporte, text="Exportar", command=self.exportar_reporte)
        btn_exportar.pack(pady=10)

    def actualizar_meses_con_registros(self):
        # Obtener la lista actualizada de meses con registros
        meses_con_registros = self.obtener_meses_con_registros()

        # Actualizar el contenido del Combobox
        self.combobox_meses['values'] = meses_con_registros

        # Establecer el mes más reciente como selección predeterminada (si hay registros)
        self.combobox_meses.set(meses_con_registros[0] if meses_con_registros else "")

    def actualizar_lista_usuarios(self):
        # Obtener la lista actualizada de usuarios
        usuarios = self.obtener_lista_usuarios()

        # Actualizar el contenido del Combobox en la pestaña "Registro"
        self.lista_usuarios['values'] = usuarios

        # Deshabilitar o habilitar las pestañas según si hay usuarios o no
        if usuarios:
            # Establecer el primer usuario como selección predeterminada
            self.lista_usuarios.set(usuarios[0])
            # Habilitar pestañas
            self.tabs.tab(self.tab_registrar, state="normal")
            self.tabs.tab(self.tab_reporte, state="normal")
        else:
            self.lista_usuarios.set("")
            # Deshabilitar pestañas
            self.tabs.tab(self.tab_registrar, state="disabled")
            self.tabs.tab(self.tab_reporte, state="disabled")

    def obtener_meses_con_registros(self):
        # Obtener todos los registros ordenados por fecha
        registros = Registro.select().order_by(Registro.fecha)

        # Obtener los meses únicos con registros
        meses_con_registros = {registro.fecha.strftime("%Y-%m") for registro in registros}

        # Ordenar los meses de mayor a menor
        meses_ordenados = sorted(meses_con_registros, reverse=True)

        return meses_ordenados

    def exportar_reporte(self):
        # Obtener el mes y rol seleccionados
        mes_seleccionado = self.combobox_meses.get()
        rol_seleccionado = self.rol_seleccionado.get()

        # Verificar que se haya seleccionado un mes
        if not mes_seleccionado:
            messagebox.showwarning("Advertencia", "Por favor, seleccione un mes.")
            return

        # Establecer un nombre predeterminado para el archivo
        nombre_predeterminado = f'Reporte_{mes_seleccionado}_{rol_seleccionado}.xlsx'

        # Mostrar el diálogo de guardado de archivo
        file_path = filedialog.asksaveasfilename(
            title="Guardar Reporte",
            defaultextension=".xlsx",
            initialfile=nombre_predeterminado,
            filetypes=[("Archivos de Excel", "*.xlsx")],
        )

        # Verificar si el usuario canceló el diálogo
        if not file_path:
            return

        try:
            # Generar y guardar el reporte de horas con el rol seleccionado
            generar_reporte_horas(file_path, rol_seleccionado, mes_seleccionado)
            messagebox.showinfo("Exportado", "El reporte se ha exportado exitosamente.")
        except Exception as e:
            messagebox.showerror("Error", f"No se pudo exportar el reporte: {str(e)}")

    def cargar_pestana_acerca_de(self):
        # Crear contenido para la pestaña "Acerca de"
        lbl_nombre_programa = tk.Label(self.tab_acerca_de, text=APPNAME, font=("Helvetica", 16, "bold"))
        lbl_version = tk.Label(self.tab_acerca_de, text=f"Versión {__version__}")
        lbl_descripcion = tk.Label(self.tab_acerca_de, text="Una aplicación para el registro de asistencia de alumnos y docentes.")
        lbl_autores = tk.Label(self.tab_acerca_de, text="\u00A9 Grupo 3 de Ingeniería de Software UCN 2023S2")  # Unicode para el símbolo de copyright

        # Configurar hipervínculo a la página de la GPL v3 en GNU.org
        link_licencia = tk.Label(self.tab_acerca_de, text="Licencia: GNU General Public License v3", fg="blue", cursor="hand2")
        link_licencia.bind("<Button-1>", lambda e: self.abrir_enlace("https://www.gnu.org/licenses/gpl-3.0.html"))

        # Configurar hipervínculo al código fuente
        link_codigo_fuente = tk.Label(self.tab_acerca_de, text="https://gitlab.com/Megver83/niditos", fg="blue", cursor="hand2")
        link_codigo_fuente.bind("<Button-1>", lambda e: self.abrir_enlace("https://gitlab.com/Megver83/niditos"))

        # Agregar el icono en la pestaña "Acerca de"
        if self.icon:
            lbl_icono = tk.Label(self.tab_acerca_de, image=self.icon)
            lbl_icono.pack(pady=10)

        # Posicionar los elementos en la pestaña "Acerca de"
        lbl_nombre_programa.pack(pady=10)
        lbl_version.pack(pady=10)
        lbl_descripcion.pack(pady=10)
        lbl_autores.pack(pady=10)
        link_codigo_fuente.pack(pady=5)
        link_licencia.pack(pady=5)

    def abrir_enlace(self, enlace):
        webbrowser.open(enlace, new=2)

    def cargar_pestana_registro(self):
        # Crear Treeview para mostrar el historial de registros
        self.tree_registros = ttk.Treeview(self.tab_registrar, columns=("Fecha", "Nombre", "Movimiento"), show="headings")
        self.tree_registros.heading("Fecha", text="Fecha")
        self.tree_registros.heading("Nombre", text="Nombre")
        self.tree_registros.heading("Movimiento", text="Movimiento")
        self.tree_registros.pack()

        # Cargar datos de registros en el Treeview
        self.cargar_registros()

        # Crear caja con lista de usuarios
        self.lista_usuarios = ttk.Combobox(self.tab_registrar, state="readonly", width=40)
        self.lista_usuarios.pack()
        self.actualizar_lista_usuarios()

        # Crear checkbox para auto detección de movimiento
        self.auto_detectar_var = tk.BooleanVar()
        chk_auto_detectar = tk.Checkbutton(self.tab_registrar, text="Auto Detectar", variable=self.auto_detectar_var, command=self.actualizar_estado_radiolist)
        chk_auto_detectar.pack(pady=10)

        # Desactivar el radiolist al inicio si el checkbox está marcado
        if self.auto_detectar_var.get():
            self.desactivar_radiolist()

        # Crear radio buttons para los movimientos
        self.movimiento_seleccionado = tk.StringVar(self.tab_registrar, value=MOVEMENTS[0])  # Establecer el primer movimiento como predeterminado
        for movimiento in MOVEMENTS:
            rb_movimiento = tk.Radiobutton(self.tab_registrar, text=movimiento, variable=self.movimiento_seleccionado, value=movimiento)
            rb_movimiento.pack()
            self.radios_movimiento.append(rb_movimiento)

        # Botones para registrar y reconocimiento facial
        btn_registrar = tk.Button(self.tab_registrar, text="Registrar", command=self.registrar_movimiento)
        btn_reconocimiento_facial = tk.Button(self.tab_registrar, text="Reconocimiento Facial", command=self.reconocimiento_facial)

        btn_registrar.pack(pady=10)
        btn_reconocimiento_facial.pack(pady=10)

    def actualizar_estado_radiolist(self):
        # Desactivar el radiolist si el checkbox está marcado
        if self.auto_detectar_var.get():
            self.desactivar_radiolist()
        else:
            self.activar_radiolist()

    def desactivar_radiolist(self):
        # Desactivar el radiolist de movimientos
        for rb_movimiento in self.radios_movimiento:
            rb_movimiento['state'] = tk.DISABLED

    def activar_radiolist(self):
        # Activar el radiolist de movimientos
        for rb_movimiento in self.radios_movimiento:
            rb_movimiento['state'] = tk.NORMAL

    def cargar_registros(self):
        # Limpiar Treeview
        for row in self.tree_registros.get_children():
            self.tree_registros.delete(row)

        # Obtener y ordenar los registros del día por fecha descendente
        registros = seleccionar_registros_por_dia(desc=True)

        # Llenar Treeview con datos de registros
        for registro in registros:
            nombre_usuario = f"{registro.rut_usuario.nombres} {registro.rut_usuario.apellidos}"
            self.tree_registros.insert("", tk.END, values=(registro.fecha, nombre_usuario, registro.movimiento))

    def obtener_lista_usuarios(self):
        # Obtener la lista de usuarios en el formato "nombres (rut)"
        lista_usuarios = [f"{usuario.nombres} {usuario.apellidos} ({usuario.rut})" for usuario in Usuario.select()]
        # Ordenar alfabéticamente la lista de usuarios
        lista_usuarios.sort()
        return lista_usuarios

    def registrar_movimiento(self):
        # Obtener el usuario seleccionado
        usuario_seleccionado = self.lista_usuarios.get()

        # Extraer el RUT del usuario seleccionado
        rut_usuario = usuario_seleccionado.split(" ")[-1][1:-1]

        # Obtener el movimiento seleccionado
        if self.auto_detectar_var.get():
            # Auto detectar el movimiento contrario al último registrado
            ultimo_movimiento = self.obtener_ultimo_movimiento(rut_usuario)
            movimiento = MOVEMENTS[1] if ultimo_movimiento == MOVEMENTS[0] else MOVEMENTS[0]
        else:
            movimiento = self.movimiento_seleccionado.get()

        # Registrar el movimiento en la base de datos
        Registro.create(rut_usuario=rut_usuario, movimiento=movimiento)

        # Actualizar la vista de registros
        self.cargar_registros()
        self.actualizar_meses_con_registros()
        messagebox.showinfo("Registro", f"Se registró el movimiento '{movimiento}' para el usuario {usuario_seleccionado}")

    def obtener_ultimo_movimiento(self, rut_usuario):
        # Obtener los registros del usuario para el día actual
        registros_usuario = seleccionar_registros_por_dia()

        # Filtrar los registros del usuario específico
        registros_usuario = [registro for registro in registros_usuario if registro.rut_usuario.rut == rut_usuario]

        # Si hay registros, devolver el último movimiento registrado
        if registros_usuario:
            return registros_usuario[-1].movimiento
        else:
            # Si no hay registros, devolver None
            return None

    def set_ultimo_usuario(self, lista_usuarios):
        known = {}
        for usuario in Usuario.select():
            known[usuario.rut] = file2bytesio(usuario.photo)

        for rut_detectado in recognize_from_webcam(known):
            # Seleccionar el último usuario de la lista
            ultimo_usuario = next(filter(lambda x: rut_detectado in x, lista_usuarios), None)

            # Establecer automáticamente el valor seleccionado en la caja de lista
            self.lista_usuarios.set(ultimo_usuario)

    def reconocimiento_facial(self):
        if self.reconocimiento_facial_thread.is_alive():
            # Mostrar un mensaje de advertencia si no hay usuarios en la lista
            messagebox.showwarning("Advertencia", "Reconocimiento facial ya está activo.")
        else:
            # Obtener la lista de usuarios en la caja de lista
            lista_usuarios = self.obtener_lista_usuarios()

            # Verificar que haya al menos un usuario en la lista
            if lista_usuarios:
                self.reconocimiento_facial_thread = threading.Thread(target=self.set_ultimo_usuario, args=(lista_usuarios,))
                self.reconocimiento_facial_thread.start()
            else:
                # Mostrar un mensaje de advertencia si no hay usuarios en la lista
                messagebox.showwarning("Advertencia", "No hay usuarios en la lista.")

    def cargar_pestana_usuarios(self):
        # Crear Treeview para mostrar la tabla de usuarios
        self.tree_usuarios = ttk.Treeview(self.tab_usuarios, columns=("Apellidos", "Nombres", "RUT", "Rol"), show="headings")
        self.tree_usuarios.heading("Apellidos", text="Apellidos")
        self.tree_usuarios.heading("Nombres", text="Nombres")
        self.tree_usuarios.heading("RUT", text="RUT")
        self.tree_usuarios.heading("Rol", text="Rol")
        self.tree_usuarios.pack()

        # Cargar datos de usuarios en el Treeview
        self.cargar_usuarios()

        # Función para agregar usuarios
        agregar_nuevo_usuario = self.abrir_popup_usuario("Agregar Usuario")

        # Botones para agregar, editar y eliminar usuarios
        btn_agregar = tk.Button(self.tab_usuarios, text="Agregar Usuario", command=agregar_nuevo_usuario)
        btn_editar = tk.Button(self.tab_usuarios, text="Editar Usuario", command=self.editar_usuario, state=tk.DISABLED)
        self.btn_eliminar = tk.Button(self.tab_usuarios, text="Eliminar Usuario", command=self.eliminar_usuario, state=tk.DISABLED)

        btn_agregar.pack(side=tk.LEFT, padx=10)
        btn_editar.pack(side=tk.LEFT, padx=10)
        self.btn_eliminar.pack(side=tk.LEFT, padx=10)

        # Configurar evento de selección en el Treeview
        self.tree_usuarios.bind('<ButtonRelease-1>', self.actualizar_estado_botones)

        # Almacenar el botón de editar en el atributo btn_editar
        self.btn_editar = btn_editar

    def actualizar_estado_botones(self, event):
        # Obtener el número de elementos seleccionados en el Treeview
        num_seleccionados = len(self.tree_usuarios.selection())

        # Habilitar o deshabilitar el botón de eliminar según la cantidad de elementos seleccionados
        if num_seleccionados == 1:
            self.btn_eliminar['state'] = tk.NORMAL
            self.btn_editar['state'] = tk.NORMAL
        else:
            self.btn_eliminar['state'] = tk.DISABLED
            self.btn_editar['state'] = tk.DISABLED

    def cargar_usuarios(self):
        # Limpiar Treeview
        for row in self.tree_usuarios.get_children():
            self.tree_usuarios.delete(row)

        # Obtener y ordenar los usuarios por apellidos y nombres
        usuarios = Usuario.select().order_by(Usuario.apellidos, Usuario.nombres)

        # Llenar Treeview con datos de usuarios
        for usuario in usuarios:
            self.tree_usuarios.insert("", tk.END, values=(usuario.apellidos, usuario.nombres, usuario.rut, usuario.rol))

    def abrir_popup_usuario(self, title, default_role=ROLES[0], default_photo=None, new=True, **entries):
        def function():
            self.popup_usuario = tk.Toplevel(self.tab_usuarios)
            self.popup_usuario.title(title)

            # Crear formulario para modificar usuario
            lbl_nombres = tk.Label(self.popup_usuario, text="Nombres:")
            lbl_apellidos = tk.Label(self.popup_usuario, text="Apellidos:")
            lbl_rut = tk.Label(self.popup_usuario, text="RUT:")
            lbl_rol = tk.Label(self.popup_usuario, text="Rol:")
            lbl_foto = tk.Label(self.popup_usuario, text="Foto:")
            lbl_pendiente = tk.Label(self.popup_usuario, text="Pendiente")

            entry_nombres = tk.Entry(self.popup_usuario)
            entry_apellidos = tk.Entry(self.popup_usuario)
            entry_rut = tk.Entry(self.popup_usuario)

            # OptionMenu para seleccionar el rol
            var_rol = tk.StringVar(self.popup_usuario)
            var_rol.set(default_role)
            option_menu_rol = tk.OptionMenu(self.popup_usuario, var_rol, *ROLES)

            # Variable para almacenar la foto seleccionada
            self.foto_seleccionada = None

            lbl_nombres.grid(row=0, column=0, padx=10, pady=10)
            lbl_apellidos.grid(row=1, column=0, padx=10, pady=10)
            lbl_rut.grid(row=2, column=0, padx=10, pady=10)
            lbl_rol.grid(row=3, column=0, padx=10, pady=10)
            lbl_foto.grid(row=4, column=0, padx=10, pady=10)
            lbl_pendiente.grid(row=4, column=1, pady=10, columnspan=2)

            entry_nombres.grid(row=0, column=1, padx=10, pady=10)
            entry_apellidos.grid(row=1, column=1, padx=10, pady=10)
            entry_rut.grid(row=2, column=1, padx=10, pady=10)
            for entry, value in entries.items():
                match entry:
                    case "names":
                        entry_nombres.insert(tk.END, value)
                    case "last_names":
                        entry_apellidos.insert(tk.END, value)
                    case "rut":
                        entry_rut.insert(tk.END, value)
                        old_rut = value
            option_menu_rol.grid(row=3, column=1, padx=10, pady=10)

            if default_photo is not None:
                # Mostrar la foto actual del usuario
                image = Image.open(file2bytesio(default_photo))
                self.actualizar_imagen(image)

            btn_subir_foto = tk.Button(self.popup_usuario, text="Subir Foto", command=self.subir_foto)
            btn_guardar_usuario = tk.Button(
                self.popup_usuario,
                text="Guardar",
                command=lambda: self.agregar_usuario(
                    entry_nombres.get(),
                    entry_apellidos.get(),
                    entry_rut.get(),
                    var_rol.get(),
                    self.popup_usuario)
                if new else self.guardar_cambios_usuario(
                    entry_nombres.get(),
                    entry_apellidos.get(),
                    entry_rut.get(),
                    old_rut,
                    var_rol.get(),
                    self.popup_usuario)
                )
            btn_subir_foto.grid(row=5, column=0, columnspan=2, pady=10)
            btn_guardar_usuario.grid(row=6, column=0, columnspan=2, pady=10)

        return function

    def subir_foto(self):
        self.filename = filedialog.askopenfilename(title="Seleccionar Foto", filetypes=[("Archivos de Imagen", "*.png *.jpg *.jpeg *.gif")], initialdir="~")
        if self.filename:
            image = Image.open(self.filename)
            self.actualizar_imagen(image)

    def actualizar_imagen(self, image):
        image = image.resize((100, 100), Image.Resampling.LANCZOS)
        self.foto_seleccionada = ImageTk.PhotoImage(image)
        foto_label = tk.Label(self.popup_usuario, image=self.foto_seleccionada)
        foto_label.grid(row=4, column=1, pady=10, columnspan=2)

    def agregar_usuario(self, nombres, apellidos, rut, rol, popup_usuario):
        try:
            nombres, apellidos, rut = preprocesar_campos(nombres, apellidos, rut)

            # Convert digital data to binary format
            image_blob = filename2blob(self.filename)

            Usuario.create(nombres=nombres, apellidos=apellidos, rut=rut, rol=rol, photo=image_blob)

            # Actualizar la lista de usuarios después de agregar uno nuevo
            self.actualizar_lista_usuarios()

            popup_usuario.destroy()
            self.cargar_usuarios()
        except Exception as e:
            messagebox.showerror("Error", f"No se pudo agregar el usuario: {str(e)}")

    def editar_usuario(self):
        # Obtener el item seleccionado en el Treeview
        seleccion = self.tree_usuarios.selection()

        if seleccion:
            # Obtener el RUT del usuario seleccionado
            rut_seleccionado = self.tree_usuarios.item(seleccion[0], 'values')[2]

            # Obtener el objeto Usuario correspondiente al RUT seleccionado
            usuario = Usuario.get(Usuario.rut == rut_seleccionado)

            # Muestra el popup
            self.abrir_popup_usuario(
                title="Editar Usuario",
                default_role=usuario.rol,
                default_photo=usuario.photo,
                new=False,
                names=usuario.nombres,
                last_names=usuario.apellidos,
                rut=usuario.rut
            )()
        else:
            # Nunca debería pasar
            messagebox.showwarning("Advertencia", "Por favor, seleccione un usuario para editar.")

    def guardar_cambios_usuario(self, nombres: str, apellidos: str, rut_nuevo: str, rut_anterior: str, rol: str, popup_editar):
        try:
            # Preprocesamiento de los campos
            nombres, apellidos, rut_nuevo = preprocesar_campos(nombres, apellidos, rut_nuevo)

            if not f"{nombres}{apellidos}".isalpha():
                raise ValueError("Los nombres y apellidos deben contener solo caracteres alfabéticos")

            usuario = Usuario.get(Usuario.rut == rut_anterior)

            # Actualizar los campos del usuario existente
            usuario.nombres = nombres
            usuario.apellidos = apellidos
            usuario.rut = rut_nuevo
            usuario.rol = rol
            try:
                if self.filename:
                    usuario.photo = filename2blob(self.filename)
            except AttributeError:
                pass

            # Guardar los cambios en la base de datos
            usuario.save()

            # Cerrar la ventana de edición
            popup_editar.destroy()

            # Actualizar la vista de usuarios
            self.cargar_usuarios()

            # Actualizar la lista de usuarios después de editar uno
            self.actualizar_lista_usuarios()

            # Mostrar mensaje de éxito
            messagebox.showinfo("Guardado", "Cambios guardados correctamente.")

        except Exception as e:
            messagebox.showerror("Error", f"No se pudieron guardar los cambios: {str(e)}")

    def eliminar_usuario(self):
        # Obtener el item seleccionado en el Treeview
        seleccion = self.tree_usuarios.selection()

        if seleccion:
            # Obtener el RUT del usuario seleccionado
            rut_seleccionado = self.tree_usuarios.item(seleccion[0], 'values')[2]

            # Mostrar un cuadro de diálogo de confirmación
            respuesta = messagebox.askyesno(
                "Confirmar Eliminación",
                "¿Está seguro de que desea eliminar al usuario? Esta acción también eliminará todos sus registros asociados."
            )

            if respuesta:
                # Realizar la eliminación en la base de datos
                try:
                    # Eliminar todos los registros asociados al usuario
                    Registro.delete().where(Registro.rut_usuario == rut_seleccionado).execute()

                    # Eliminar al usuario
                    Usuario.delete().where(Usuario.rut == rut_seleccionado).execute()

                    # Actualizar la lista de usuarios después de eliminar uno
                    self.actualizar_lista_usuarios()

                    self.cargar_usuarios()
                    messagebox.showinfo("Eliminado", "Usuario y registros asociados eliminados correctamente.")
                except Exception as e:
                    messagebox.showerror("Error", f"No se pudo eliminar el usuario: {str(e)}")
        else:
            # No debería ocurrir nunca
            messagebox.showwarning("Advertencia", "Por favor, seleccione un usuario para eliminar.")


def main():
    root = tk.Tk()
    app = InterfazNiditos(root)
    root.mainloop()
