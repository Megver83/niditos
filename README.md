# Niditos

Niditos es un software para el registro de asistencia de alumnos y docentes. Utiliza opcionalmente reconocimiento facial.

## Instalación y ejecución

Instala sus dependencias en un entorno virtual:

```
$ python -m venv env
$ source env/bin/activate
$ pip install -r requirements.txt
```

Luego, ejecútalo:

```
$ python -m niditos
```

## Notas

* Si se cambia el rut de un usuario, sus registros no serán exportados en una hoja propia
* Eliminar un usuario implica eliminar sus registros

## TODO:

* [x] Mostrar tamaño de la base de datos
* [x] Opcion para limpiar registros de la BD
* [ ] Opción de eliminar un registro-movimiento en particular
* [ ] try-except cuando el reconocimiento facial falle
* [x] Solo mostrar registros del día
* [x] Generar reportes Excel
* [ ] Advertir cuando se intente registrar un movimiento si el anterior fue el contrario
* [x] Añadir la opción "Autodetectar" movimiento
* [x] CI/CD
    * [x] Empaquetamiento con pyInstaller
* [ ] Opción para escoger la webcam a usar para el reconocimiento facial
